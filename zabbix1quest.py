import csv
import requests
import json



ZABIX_ROOT = 'http://5.53.126.148/'
url = ZABIX_ROOT + 'api_jsonrpc.php'
headers = {
    'content-type': 'application/json-rpc',
}

# авторизация
payload = {
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        'user': 'Admin',
        'password': 'zabbix',
    },
    # "auth": None,
    "id": 1,
}
headers = {
    'content-type': 'application/json-rpc',
}
res = requests.post(url, data=json.dumps(payload), headers=headers)
print (type(res))
res = res.json()
print('user.login response')
print(json.dumps(res, indent=2))
# add.group
payload = {
            "jsonrpc": "2.0",
            "method": "hostgroup.massadd",
            "params": {
                "groups": [{}],
                "hosts": [{}],
                'output': ['hostid',
                    'name'],
            },
            "auth": res['result'],
            "id": 1
        }
contragents_tmp = dict()
with open('123.csv', encoding='utf-8') as csvfile:  # открыли  csv с помощью ридера
    reader = csv.DictReader(csvfile)
    for row in reader:  # проход по всем строкам csv
        print(row['complex_name'], row['contragent_name'])  # печать каждой строки (два столбца)
        if (row['contragent_name'] == '') or (row['contragent_name'] == 'ВЫВЕДЕН'):
            continue
        try:
            contragents_tmp[row['contragent_name']].append(row['complex_name'])
        except:
            contragents_tmp[row['contragent_name']] = [row['complex_name']]
    for contragent, hosts in contragents_tmp.items():
        host_dict = [{"hostid": x} for x in hosts]

        payload["params"]["groups"] = [{"gropid": contragent}]
        payload["params"]["hosts"] = host_dict
        res = requests.post(url, data=json.dumps(payload), headers=headers)
        res2 = res.json()
        print('host.get response')
        print(json.dumps(res2, indent=2))



