# -*- coding: utf-8 -*-
import csv
import requests
import json
from pyzabbix import ZabbixMetric, ZabbixSender
from datetime import date
import datetime as DT
import time
from ast import literal_eval

today = date.today()

# env
ZABIX_ROOT = 'http://5.53.126.108/'
url = ZABIX_ROOT + 'api_jsonrpc.php'
# авторизация, получаем ключ для авторизации
headers = {
    'content-type': 'application/json-rpc',
}
payload = {
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        'user': 'Admin',
        'password': 'zabbix',
    },
    # "auth": None,
    "id": 1,
}
headers = {
    'content-type': 'application/json-rpc',
}
res = requests.post(url, data=json.dumps(payload), headers=headers)
print (type(res))
res = res.json()
print('user.login response')
print(str(json.dumps(res, indent=2)).encode('utf-8'))
auth_key=res['result']

# создаем на основе файла сsv два словаря с которыми будем в последующем работать dict contragents_tmp, dict items_tmp
contragents_tmp = dict() # словарь с хостами и группами из файла
items_tmp=dict() #словарь с хостами из файла
with open('123.csv', encoding='utf-8') as csvfile:  # открыли  csv с помощью ридера
    reader = csv.DictReader(csvfile)
    for row in reader:  # проход по всем строкам csv
        # print(row['complex_name'], row['contragent_name'])  # печать каждой строки (два столбца)
        if (row['contragent_name'] == '') or (row['contragent_name'] == 'ВЫВЕДЕН'):
            continue
        elif row['complex_ips'] == '':
            continue
        try:
            items_tmp[row['complex_name']]=[row['complex_name'],row['top'],row['yield'],row['contragent_name'],row[''],row['complex_model_name '],row['cafap_status_date'],row['complex_ips']]
            contragents_tmp[row['contragent_name']].append(row['complex_name'])
        except:
            contragents_tmp[row['contragent_name']] = [row['complex_name']]
print (items_tmp)

# создаем запрос group.get для получения групп и groupid и записи его в словарь group_tmp1
group_tmp=dict()
pay_group_get =    {
        "jsonrpc": "2.0",
        "method": "hostgroup.get",
        "params": {
            "output": "extend",
            "filter": {
                "name": []
            }
        },
        "auth": auth_key,
        "id": 1
}
res2 = requests.post(url, data=json.dumps(pay_group_get), headers=headers)
res2 = res2.json()
print('host.get response_GROUPS')
# print(json.dumps(res2, indent=2))
for groups in res2['result']:
   group_tmp[groups['name']]=groups['groupid']
print(group_tmp)

#создаем словарь в который записываем hostid и groupid хранящиеся на сервере
server_hosts_groups=dict() #Словарь в котором прописаны ID хоста и принадлеждность к группам и именам групп вида {hostid:{gropid:group.name, gropid:group.name}
id_host=dict() #словарь вида hostname:host.id
interfaceID_hostID=dict()# вида host.id:interface.idсловарь интерфейсов
pay_get_all_hosts_all_groups = {
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output":["hostid","host"],
        "selectGroups": "extend",
        "selectInterfaces": "extend",
        "filter": {
            "host": [
            ]
        }
    },
    "auth": auth_key,
    "id": 2
}

res = requests.post(url, data=json.dumps(pay_get_all_hosts_all_groups), headers=headers)
res = res.json()
print('host.get response_host&groups')
# print(json.dumps(res, indent=2))
for hosts in res['result']:
    # print(hosts)
    id_host[hosts['host']] = hosts['hostid']
    interfaceID_hostID[hosts['hostid']]= hosts['interfaces'][0]['interfaceid']
    for group in hosts['groups']:
        try:
            server_hosts_groups[hosts['hostid']][group['groupid']]=group['name']
        except:
            server_hosts_groups[hosts['hostid']]={group['groupid']: group['name']}
print (server_hosts_groups)
print (id_host)
print (interfaceID_hostID)


for item in items_tmp.items():
    date_old = DT.datetime.strptime(item[1][6], "%m/%d/%Y").date() #считаем дни
    UPTIME_DAYS=((today-date_old).days)
    # Добавление новой метрики
    pay_create_item = {
        "jsonrpc": "2.0",
        "method": "item.create",
        "params": {
            "name": "DEVICE_DAYS_UPTIME",
            "key_": "UPTIME_DATE",
            "hostid": id_host[item[0]],
            "type": 2,
            "interfaceid": interfaceID_hostID[id_host[item[0]]],
            "value_type": 3,
            "delay": "60s",
            "units": "days",
        },
        "auth": auth_key,
        "id": 1
    }
    res5 = requests.post(url, data=json.dumps(pay_create_item), headers=headers)
    res5 = res5.json()
    print('item.create NEWMETRICS')
    print(json.dumps(res5, indent=2))

# time.sleep(10)
for item in items_tmp.items():
    date_old = DT.datetime.strptime(item[1][6], "%m/%d/%Y").date()  # считаем дни
    UPTIME_DAYS = ((today - date_old).days)
    metrics = [ZabbixMetric(item[0], 'UPTIME_DATE', UPTIME_DAYS)]
    print (metrics)
    zbx = ZabbixSender('37.200.70.4')
    res = zbx.send(metrics)
    print(res)
# отправка значения date
# packet = [
#   ZabbixMetric('1809046-1809051', 'UPTIME_DATE', '50.1'),
#   ZabbixMetric('AS5000102', 'UPTIME_DATE', '50.1'),
# ]
# result = ZabbixSender(use_config=True).send(packet)




# ZabbixSender(self, zabbix_server='37.200.70.4',
#                  zabbix_port=10051,
#                  use_config=None,
#                  chunk_size=250,
#                  socket_wrapper=None,
#                  timeout=10).send(metrics)

# packet = ZabbixPacket()
# packet.add('1809046-1809051','UPTIME_DATE', '50')
# packet.add('AS5000102', 'UPTIME_DATE', '51')
# server.send(packet)
# print(server.status)
#


# #сравнение данных на сервере с данными в файле, перебор по всем значениям
# delete_host_from_group=False
# for item in items_tmp.items():
#     pay_update = {
#         "jsonrpc": "2.0",
#         "method": "host.update",
#         "params": {
#             "hostid": '',
#             "groups":''
#         },
#         "auth": auth_key,
#         "id": 1
#     }
#     delete_host_from_group=False
#     deleting_groups=[]
#     for gropids in server_hosts_groups[id_host[item[1][0]]].keys():
#         if gropids!=group_tmp[item[1][3]]:
#             print(gropids)
#             print(group_tmp[item[1][3]])
#             print('kill')
#             deleting_groups.append(gropids)
#             pay_update["params"]["hostid"]=id_host[item[1][0]]
#             pay_update["params"]["groups"]=[group_tmp[item[1][3]]]
#             delete_host_from_group=True
#             print(pay_update)
#     if delete_host_from_group==True:
#         res4 = requests.post(url, data=json.dumps(pay_update), headers=headers)
#         res4 = res4.json()
#         print('host.get response_host&groups')
#         print(json.dumps(res4, indent=2))
#         if "error" in res4:
#             print ('Error', res4['error'])
#         elif "error" not in res4 and "hostids" in res4:
#             print('Успешно удалено из группы=',deleting_groups,'Host=',res4["result"]["hostids"], 'теперь в группе=', group_tmp[item[1][3]] )
#

