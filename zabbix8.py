import csv
import requests
import json

# Прочитать все IP адреса из поля complex_ips, проверить совпадают ли с текущими IP адресами соответствующего хоста (имя хоста в Zabbix соответствует полю complex_name из входных данных) , если не совпадает, то актуализировать интерфейсы и их IP-адреса, как по числу интерфейсов, так и по корректности  IP-адресов;
# env
ZABIX_ROOT = 'http://185.187.90.244/'
url = ZABIX_ROOT + 'api_jsonrpc.php'
# авторизация, получаем ключ для авторизации
headers = {
    'content-type': 'application/json-rpc',
}
payload = {
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        'user': 'Admin',
        'password': 'zabbix',
    },
    # "auth": None,
    "id": 1,
}
headers = {
    'content-type': 'application/json-rpc',
}
res = requests.post(url, data=json.dumps(payload), headers=headers)
res = res.json()
print('user.login response')
# print(str(json.dumps(res, indent=2)).encode('utf-8'))
auth_key=res['result']
#создаем на основе файла сsv два словаря с которыми будем в последующем работать dict contragents_tmp, dict items_tmp
contragents_tmp = dict() # словарь с хостами и группами из файла
items_tmp=dict() #словарь с хостами из файла
with open('123.csv', encoding='utf-8') as csvfile:  # открыли  csv с помощью ридера
    reader = csv.DictReader(csvfile)
    for row in reader:  # проход по всем строкам csv
        # print(row['complex_name'], row['contragent_name'])  # печать каждой строки (два столбца)
        if (row['contragent_name'] == '') or (row['contragent_name'] == 'ВЫВЕДЕН'):
            continue
        elif row['complex_ips'] == '':
            continue
        try:
            items_tmp[row['complex_name']]=[row['complex_name'],row['top'],row['yield'],row['contragent_name'],row[''],row['complex_model_name '],row['cafap_status_date'],row['complex_ips']]
            contragents_tmp[row['contragent_name']].append(row['complex_name'])
        except:
            contragents_tmp[row['contragent_name']] = [row['complex_name']]
print (items_tmp)

# создаем запрос group.get для получения групп и groupid и записи его в словарь group_tmp1
group_tmp=dict()
pay_group_get =    {
        "jsonrpc": "2.0",
        "method": "hostgroup.get",
        "params": {
            "output": "extend",
            "filter": {
                "name": []
            }
        },
        "auth": auth_key,
        "id": 1
}
res2 = requests.post(url, data=json.dumps(pay_group_get), headers=headers)
res2 = res2.json()
print('host.get response_GROUPS')
# print(json.dumps(res2, indent=2))
for groups in res2['result']:
   group_tmp[groups['name']]=groups['groupid']
print(group_tmp)

#создаем словарь в который записываем hostid и groupid хранящиеся на сервере
server_hosts_groups=dict() #Словарь в котором прописаны ID хоста и принадлеждность к группам и именам групп вида {hostid:{gropid:group.name, gropid:group.name}
id_host=dict() #словарь вида hostname:host.id
interfaceID_hostID=dict()# вида host.id:[{interface:}.idсловарь интерфейсов


pay_get_all_hosts_all_groups = {
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output":["hostid","host"],
        "selectGroups": "extend",
        "selectInterfaces": "extend",
        "filter": {
            "host": [
            ]
        }
    },
    "auth": auth_key,
    "id": 2
}

res = requests.post(url, data=json.dumps(pay_get_all_hosts_all_groups), headers=headers)
res = res.json()
print('host.get response_host&groups')
# print(json.dumps(res, indent=2))
for hosts in res['result']:
    id_host[hosts['host']] = hosts['hostid']
    # print (hosts['interfaces'])
    interfaceID_hostID[hosts['hostid']]= hosts['interfaces']
    for group in hosts['groups']:
        try:
            server_hosts_groups[hosts['hostid']][group['groupid']]=group['name']
        except:
            server_hosts_groups[hosts['hostid']]={group['groupid']: group['name']}
print (server_hosts_groups)
print (id_host)
print (interfaceID_hostID)
id_host_revers={}
for host in id_host.keys():
    try:
        id_host_revers[id_host[host]]=host
    except:
        id_host_revers[id_host[host]] = {id_host[host]: host}
print (id_host_revers)
# print (items_tmp)



for host_ids in interfaceID_hostID: #Перебираем все  hostid  на сервере
    server_host_interface = []
    list_host_interface = []
    a = []
    b = []
    ip_in_list = False
    a = interfaceID_hostID [host_ids]
    for ips in a:                                #создаем список из  ip - адресов хоста на сервере
        server_host_interface.append(ips["ip"])

    if id_host_revers[host_ids] in items_tmp.keys(): #если хост есть есть на сервере и присутствует в списке
        b = json.loads(items_tmp[id_host_revers[host_ids]][7])
        not_in_list = False
        for ips in b:                                   #создаем список из  ip - адресов хоста в списке
            list_host_interface.append(ips["ip"])
    else:
        print("Хост есть на сервере, но отсутствует в списке")
        not_in_list=True
        continue
    print(list_host_interface,server_host_interface)
    if list_host_interface == server_host_interface:            #проверка на совпадение
        print("Совпадает")
    elif list_host_interface!=[]:
        print("НЕСовпадает")
        payload_hosts = {
            "jsonrpc": "2.0",
            "method": "hostinterface.replacehostinterfaces",
            "params": {
                "hostid": "",
                "interfaces": [
                ]
            },
            "auth": auth_key,
            "id": 1
        }
        for ips in b:                                                           #извлекаем списко  ip- адресов хоста из списка
            if ips['order'] == 1:
                actual_ip = ips['ip']
                payload_hosts["params"]["interfaces"].append(
                    {"dns": "", "ip": actual_ip, "main": 1, "port": "10050", "type": 1, "useip": 1})
            else:
                other_ip = ips['ip']
                payload_hosts["params"]["interfaces"].append(
                    {"dns": "", "ip": other_ip, "main": 0, "port": "10050", "type": 1, "useip": 1})
        payload_hosts["params"].update({"hostid": host_ids})
        print(payload_hosts)
        res2 = requests.post(url, data=json.dumps(payload_hosts), headers=headers)
        res2 = res2.json()
        print('host.get response')
        print(json.dumps(res2, indent=2))
