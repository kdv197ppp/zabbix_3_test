import csv
import requests
import json
from ast import literal_eval



ZABIX_ROOT = 'http://37.200.70.4/'
url = ZABIX_ROOT + 'api_jsonrpc.php'
headers = {
    'content-type': 'application/json-rpc',
}

# авторизация
payload = {
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        'user': 'Admin',
        'password': 'zabbix',
    },
    # "auth": None,
    "id": 1,
}
headers = {
    'content-type': 'application/json',
}
res = requests.post(url, data=json.dumps(payload), headers=headers)
print (type(res))
res = res.json()
print('user.login response')
print(json.dumps(res, indent=2))

# add.group
payload = {
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "",
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "",
                "dns": "",
                "port": "10050"
            }
        ],
        "groups": [
            {
                "groupid": "50"
            }
        ],
        "tags": [
            {
                "tag": "Host name",
                "value": "Linux server"
            }
        ],
        "templates": [
            {
            }
        ],
        "macros": [
            {
                "macro": "{$USER_ID}",
                "value": "123321"
            }
        ],
        "inventory_mode": 0,
        "inventory": {
            "macaddress_a": "01234",
            "macaddress_b": "56768"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
contragents_tmp = dict()
host_name_new ='AS5000004'
with open('123.csv', encoding='utf-8') as csvfile:  # открыли  csv с помощью ридера
    reader = csv.DictReader(csvfile)
    # print(reader.fieldnames)
    for row in reader:  # проход по всем строкам csv
        if (row['complex_name'] == host_name_new):
            if (row['contragent_name'] == '') or (row['contragent_name'] == 'ВЫВЕДЕН'): #провера комплекса на валидность
                print('КОМПЛЕКС ВЫВЕДЕН')
                continue
            elif row['complex_ips'] == '':
                print('НЕТ СЕТЕВОГО ИНТЕРФЕЙСА')
                continue
            a=literal_eval(row['complex_ips'])#вывод актуального ip в переменную actual_ip
            for ips in a:
                if ips['order']==1:
                    actual_ip=ips['ip']
            payload["params"]["host"]=row['complex_name']
            payload["params"]["templates"]=[{"templateid": row["complex_model_name "]}]
            payload["params"]["interfaces"][0].update({"ip":  actual_ip})
            print(payload)

        # res = requests.post(url, data=json.dumps(payload), headers=headers)
        # res2 = res.json()
        # print('host.get response')
        # print(json.dumps(res2, indent=2))


