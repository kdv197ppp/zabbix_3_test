import csv
import requests
import json
from ast import literal_eval



ZABIX_ROOT = 'http://185.187.90.244/'
url = ZABIX_ROOT + 'api_jsonrpc.php'
headers = {
    'content-type': 'application/json-rpc',
}

# авторизация
payload = {
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        'user': 'Admin',
        'password': 'zabbix',
    },
    # "auth": None,
    "id": 1,
}
headers = {
    'content-type': 'application/json-rpc',
}
res = requests.post(url, data=json.dumps(payload), headers=headers)
print (type(res))
res = res.json()
print('user.login response')
print(json.dumps(res, indent=2))
auth_key=res['result']
# add.group
# payload = {
#             "jsonrpc": "2.0",
#             "method": "hostgroup.massadd",
#             "params": {
#                 "groups": [{}],
#                 "hosts": [{}],
#                 'output': ['hostid',
#                     'name'],
#             },
#             "auth": auth_key,
#             "id": 1
#         }
# contragents_tmp = dict()
# with open('123.csv', encoding='utf-8') as csvfile:  # открыли  csv с помощью ридера
#     reader = csv.DictReader(csvfile)
#     for row in reader:  # проход по всем строкам csv
#         print(row['complex_name'], row['contragent_name'])  # печать каждой строки (два столбца)
#         if (row['contragent_name'] == '') or (row['contragent_name'] == 'ВЫВЕДЕН'):
#             continue
#         try:
#             contragents_tmp[row['contragent_name']].append(row['complex_name'])
#         except:
#             contragents_tmp[row['contragent_name']] = [row['complex_name']]
#     for contragent, hosts in contragents_tmp.items():
#         host_dict = [{"hostid": x} for x in hosts]
#
#         payload["params"]["groups"] = [{"gropid": contragent}]
#         payload["params"]["hosts"] = host_dict
#         res = requests.post(url, data=json.dumps(payload), headers=headers)
#         res2 = res.json()
#         print('host.get response')
#         print(json.dumps(res2, indent=2))


#create dict contragents dict items

contragents_tmp = dict()
items_tmp=dict()
with open('123.csv', encoding='utf-8') as csvfile:  # открыли  csv с помощью ридера
    reader = csv.DictReader(csvfile)
    for row in reader:  # проход по всем строкам csv
        # print(row['complex_name'], row['contragent_name'])  # печать каждой строки (два столбца)
        if (row['contragent_name'] == '') or (row['contragent_name'] == 'ВЫВЕДЕН'):
            continue
        elif row['complex_ips'] == '':
            continue
        # "complex_name", "top", "yield", "contragent_name", "", "complex_model_name ", "cafap_status_date", "complex_ips"

        try:
            items_tmp[row['complex_name']]=[row['complex_name'],row['top'],row['yield'],row['contragent_name'],row[''],row['complex_model_name '],row['cafap_status_date'],row['complex_ips']]
            contragents_tmp[row['contragent_name']].append(row['complex_name'])
        except:
            contragents_tmp[row['contragent_name']] = [row['complex_name']]

# создаем запрос group.get для получения groupid и записи его в словарь group_tmp
print('items_tmp', items_tmp)
group_tmp=dict()
pay_group_get =    {
        "jsonrpc": "2.0",
        "method": "hostgroup.get",
        "params": {
            "output": "extend",
            "filter": {
                "name": [
                ]
            }
        },
        "auth": auth_key,
        "id": 1
}
for contragent, hosts in contragents_tmp.items():
    # print (contragent)
    pay_group_get["params"]["filter"]["name"].append(contragent)
    group_tmp[contragent] = ''
    # print(pay_group_get)
    # print(group_tmp)
res2 = requests.post(url, data=json.dumps(pay_group_get), headers=headers)
res2 = res2.json()
print('host.get response')
# print(json.dumps(res2, indent=2))
i=0
for contragent in group_tmp:
    group_tmp[contragent]=res2['result'][i]["groupid"]
    i=i+1
print(group_tmp)

#Добавление хостов
for item in items_tmp.items():
    # print(item[1][7])
    payload_hosts = {
        "jsonrpc": "2.0",
        "method": "host.create",
        "params": {
            "host": "",
            "interfaces": [
                {
                    "type": 1,
                    "main": 1,
                    "useip": 1,
                    "ip": "",
                    "dns": "",
                    "port": "10050"
                }
            ],
            "groups": [
            ],
        },
        "auth": auth_key,
        "id": 1
    }
    a = literal_eval(item[1][7])  # вывод актуального ip в переменную actual_ip
    for ips in a:
         if ips['order'] == 1:
             actual_ip = ips['ip']
             payload_hosts["params"]["interfaces"].append({"type": 1,"main": 1, "useip": 1, "ip": actual_ip, "dns": "", "port": "10050" })
         else:
             other_ip = ips['ip']
             payload_hosts["params"]["interfaces"].append({"type": 1,"main": 0, "useip": 1, "ip": other_ip, "dns": "", "port": "10050" })
    payload_hosts["params"].update({"host": item[1][0]})
    payload_hosts["params"]["groups"].append({"groupid": group_tmp[item[1][3]]})
    print(payload_hosts)
    res2 = requests.post(url, data=json.dumps(payload_hosts), headers=headers)
    res2 = res2.json()
    print('host.get response')
    print(json.dumps(res2, indent=2))
