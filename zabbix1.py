import csv
import requests
import json

# name_array=[] #массив названий групп
# complex_array=[[''],['']] # массив названий группы / комплекса

ZABIX_ROOT = 'http://localhost/zabbix'
url = ZABIX_ROOT + '/api_jsonrpc.php'
headers = {
    'content-type': 'application/json',
}
########################################
# авторизация
########################################
payload = {
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        'user': 'api',
        'password': 'api',
    },
    "auth": None,
    "id": 0,
}
headers = {
    'content-type': 'application/json',
}
#res = requests.post(url, data=json.dumps(payload), headers=headers)
#res = res.json()
#print('user.login response')
#print(json.dumps(res, indent=2))

########################################
# add.group
########################################
payload = {
            "jsonrpc": "2.0",
            "method": "hostgroup.massadd",
            "params": {
                "groups": [{}],
                "hosts": [{}],
                'output': ['hostid',
                    'name'],
            },
            "auth": "f223adf833b2bf2ff38574a67bba6372",
            "id": 1
        }
contragents_tmp = dict()
with open('H:\PycharmProjects\untit\123.csv') as csvfile:  # открыли  csv с помощью ридера
    reader = csv.DictReader(csvfile)
    for row in reader:  # проход по всем строкам csv
        print(row['complex_name'], row['contragent_name'])  # печать каждой строки (два столбца)
        if (row['contragent_name'] == '') or (row['contragent_name'] == 'ВЫВЕДЕН'):
            continue
        try:
            contragents_tmp[row['contragent_name']].append(row['complex_name'])
        except:
            contragents_tmp[row['contragent_name']] = [row['complex_name']]
    for contragent, hosts in contragents_tmp.items():
        host_dict = [{"hostid": x} for x in hosts]

        payload["params"]["groups"] = [{"gropid": contragent}]
        payload["params"]["hosts"] = host_dict
        res = requests.post(url, data=json.dumps(payload), headers=headers)
        res2 = res.json()
        print('host.get response')
        print(json.dumps(res2, indent=2))



